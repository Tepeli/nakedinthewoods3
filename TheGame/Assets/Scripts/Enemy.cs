﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Enemy : MonoBehaviour
{
    Animator m_Animator;
    public float range;
    public float enemyHealth = 2;
    public float ballDamage = 1;
    public Transform Player;
    int MoveSpeed = 5;
    int MaxDist = 10;
    int MinDist = 0;
    public AudioClip Sound;
    public GameEnding gameEnding;
    bool isDying = false;

    void Start()
    {
        
        m_Animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (Vector3.Distance(Player.position, transform.position) <= range)
        {
            transform.LookAt(Player);

            if (Vector3.Distance(transform.position, Player.position) >= MinDist)
            {
                if (!isDying)
                {
                    transform.position += transform.forward * MoveSpeed * Time.deltaTime;
                }



                if (Vector3.Distance(transform.position, Player.position) <= MaxDist)
                {
                    //Here Call any function U want Like Shoot at here or something
                }

            }
        }

      

    }
    void OnTriggerEnter(Collider other)
    {
     
        if (other.gameObject.tag == "Projectile")
        {
            Destroy(other.gameObject);
            
            if (enemyHealth <= 0)
            {
                AudioSource.PlayClipAtPoint(Sound, this.gameObject.transform.position);
                m_Animator.SetTrigger("Dying");
                isDying = true;



            }
            else
            {
                enemyHealth-= ballDamage;
            }
            
        } else if (other.gameObject.tag == "Player")
        {
            m_Animator.SetTrigger("Attack");
            gameEnding.CaughtPlayer();
        }
    }

    void Die()
    {
        Destroy(this.gameObject);
        
    }
}