﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
   
    public float movementSpeed = 10;
    public float turningSpeed = 60;
    public float shootForce = 10;
    public float ballLifetime= 1;
    public GameObject projectile;
    public bool isSawing;
    public int PlayerHealth = 5;
    public int damage = 1;
    bool TakingDmg;
    AudioSource Sound;
    public GameEnding gameEnding;

    Animator m_Animator;
    float horizontal;
    float vertical;
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        Sound = GetComponent<AudioSource>();

    }

    void Update()
    {
        bool IsKicking = Input.GetButtonDown("Fire2");
        bool Sawing = Input.GetButtonDown("Fire1");

        if (!IsKicking && !Sawing)
        {
            
            horizontal = Input.GetAxis("Horizontal") * turningSpeed * Time.deltaTime;
            transform.Rotate(0, horizontal, 0);

            vertical = Input.GetAxis("Vertical") * movementSpeed * Time.deltaTime;
            transform.Translate(0, 0, vertical);


        } else if (IsKicking && !Sawing)
        {
            m_Animator.SetTrigger("Attacking");     
            horizontal = 0;
            vertical = 0;
        } else if (!IsKicking && Sawing)
        {
            StartCoroutine(SideSwingCo());

        }


        bool hasHorizontalInput = horizontal > 0;
        bool hasVerticalInput = vertical > 0;
        bool hasVerticalInputBack = vertical < 0;

        bool isWalking =  hasVerticalInput;
        bool isWalkingBackwards = hasVerticalInputBack;

        m_Animator.SetBool("IsWalking", isWalking);
        m_Animator.SetBool("IsWalkingBackwards", isWalkingBackwards);
        

    }

    private IEnumerator SideSwingCo()
    {
        isSawing = true;
        isSawing = true;
        m_Animator.SetTrigger("Sawing");
        horizontal = 0;
        vertical = 0;
        // Wait for 1 second
       
        yield return new WaitForSeconds(1);
        isSawing = false;
    }

    void Throw()
    {
        GameObject shot = GameObject.Instantiate(projectile, transform.position + (transform.forward * 2), transform.rotation);
        shot.GetComponent<Rigidbody>().AddForce(transform.forward * shootForce);
        shot.transform.position += Vector3.up;
        Destroy(shot, ballLifetime);
    }

    void Die()
    {
        gameEnding.CaughtPlayer();
    }
    void Hit()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(PlayerHealth+" "+TakingDmg);
        if (other.gameObject.tag == "Axe" || other.gameObject.tag == "Projectile")
        {
            if (TakingDmg == false)
            {
                PlayerHealth -= damage;
                Sound.Play();
                StartCoroutine(DmgTimer());
            }

        }

        if (PlayerHealth <= 0)
        {
            m_Animator.SetTrigger("Dying");
        }
    }

    private IEnumerator DmgTimer()
    {
        TakingDmg = true;
        // Wait for 1 second
        yield return new WaitForSeconds(1);
        TakingDmg = false;
    }
}

