﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Girl : MonoBehaviour
{
    public float range;
    public Transform Player;
    public int MoveSpeed = 3;
    float MinDist = 2f;
    Animator m_Animator;
    public GameEnding gameEnding;

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
    if (Vector3.Distance(Player.position, transform.position) <= range)
        {
            transform.LookAt(Player);
            gameEnding.WithGirl();
            if (Vector3.Distance(transform.position, Player.position) >= MinDist)
            {

                transform.position += transform.forward* MoveSpeed * Time.deltaTime;
               
                m_Animator.SetBool("GirlRun", true);
                



            } else
            {
                 
                m_Animator.SetBool("GirlRun", false);
                


            }

        
        }
        else
        {
           
            m_Animator.SetBool("GirlRun", false);
            gameEnding.LostGirl();
        }

    }
}
