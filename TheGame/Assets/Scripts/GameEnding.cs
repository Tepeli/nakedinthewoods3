﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{
    public int range = 20;
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public GameObject player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;
    GameObject[] enemies;

    bool m_IsChased;
    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    bool m_IsWithGirl;
    float m_Timer;
    bool m_HasAudioPlayed;

    
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            m_IsPlayerAtExit = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            m_IsPlayerAtExit = false;
        }
    }

    public void CaughtPlayer()
    {
        m_IsPlayerCaught = true;
    }

    public void WithGirl()
    {
        m_IsWithGirl = true;
    }

    public void LostGirl()
    {
        m_IsWithGirl = false;
    }



    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("Menu");
        }

        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject e in enemies)
        {
            
            if (Vector3.Distance(e.transform.position, player.transform.position) <= range)
            {

                m_IsChased = true;
                Debug.Log(m_IsChased);
            }

        }
        

        if (m_IsPlayerAtExit && m_IsWithGirl && !m_IsChased)
        {
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
        }
        else if (m_IsPlayerCaught)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }

        m_IsChased = false;
    }

    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        if (!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }

        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;

        if (m_Timer > fadeDuration + displayImageDuration)
        {
            if (doRestart)
            {
                SceneManager.LoadScene("Menu");
            }
            else
            {
                
                if (SceneManager.GetActiveScene().name == "Level2") {
                    Debug.Log("open menu");
                    SceneManager.LoadScene("Menu");
                } else
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                }
            }
        }
    }
}
