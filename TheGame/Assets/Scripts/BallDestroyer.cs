﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BallDestroyer : MonoBehaviour
{

    
    public AudioClip audio;
    private void Start()
    {
        
    }
    void OnTriggerEnter(Collider other)
    {
        
        AudioSource.PlayClipAtPoint(audio, this.gameObject.transform.position, 0.5f);
        
        if (other.gameObject.tag != "Enemy")
        {
            
            Destroy(this.gameObject);
        }
    }
}
