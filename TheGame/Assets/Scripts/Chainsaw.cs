﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chainsaw : MonoBehaviour
{
    AudioSource Sound;
    // Start is called before the first frame update
    void Start()
    {
        Sound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        bool Sawing = Input.GetButtonDown("Fire1");
        if (Sawing)
        {
            Sound.Play();
        }
    }
}
