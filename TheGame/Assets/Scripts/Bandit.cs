﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Bandit : MonoBehaviour
{
    public float range;
    public Transform Player;
    int MoveSpeed = 3;
    int MaxDist = 10;
    float MinDist = 1.5f;
    bool closeEnough;
    public float Cooldown = 3;
    float TimerForNextAttack;
    public float enemyHealth = 2;
    public float ballDamage = 1;
    

    public float shootForce = 10;
    public float ballLifetime = 1;
    public GameObject projectile;
    bool isSawing;

    Animator m_Animator;
    AudioSource Sound;

    void Start()
    {

        Sound = GetComponent<AudioSource>();
        
        m_Animator = GetComponent<Animator>();
        TimerForNextAttack = 0;
        
    }

    void Update()
    {
        
        if (Vector3.Distance(Player.position, transform.position) <= range)
        {
            transform.LookAt(Player);
           

            if (Vector3.Distance(transform.position, Player.position) >= MinDist)
            {

                transform.position += transform.forward * MoveSpeed * Time.deltaTime;
               
                m_Animator.SetBool("BanditRun", true);


                closeEnough = false;

            } else
            {
                closeEnough = true;
                
                m_Animator.SetBool("BanditRun", false);
                m_Animator.SetTrigger("BanditSwing");
            }

            if (Vector3.Distance(transform.position, Player.position) <= MaxDist && closeEnough == false)
            {
                if (TimerForNextAttack > 0)
                {
                    TimerForNextAttack -= Time.deltaTime;
                }
                else if (TimerForNextAttack <= 0)
                {
                    m_Animator.SetTrigger("BanditAttack");
                    TimerForNextAttack = Cooldown;
                }
                 
            }
        }
        else
        {
           
            m_Animator.SetBool("BanditRun", false);
            

        }

    }

    void Throw()
    {
        GameObject shot = GameObject.Instantiate(projectile, transform.position + (transform.forward * 2), transform.rotation);
        shot.GetComponent<Rigidbody>().AddForce(transform.forward * shootForce);
        shot.transform.position += Vector3.up;
        Destroy(shot, ballLifetime);
    }

    void OnTriggerEnter(Collider other)
    {
        isSawing = Player.GetComponent<Player>().isSawing;
       
        if (other.gameObject.tag == "Projectile")
        {
            Destroy(other.gameObject);

            TakeDamage();

        } else if (other.gameObject.tag == "Weapon" && isSawing)
        {
            TakeDamage();
        }
    }

    void TakeDamage()
    {
        if (enemyHealth <= 0)
        {
            m_Animator.SetTrigger("Dying");
            Sound.Play();
        }
        else
        {
            enemyHealth -= ballDamage;
        }
    }
    void Die()
    {
        
        Destroy(this.gameObject);

    }
    void Hit()
    {

    }

}